server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name olki.example.org;

  # For example with certbot (you need a certificate to run https)
  ssl_certificate      /etc/letsencrypt/live/olki.example.org/fullchain.pem;
  ssl_certificate_key  /etc/letsencrypt/live/olki.example.org/privkey.pem;

  # Security hardening (as of 11/02/2018)
  ssl_protocols TLSv1.2; # TLSv1.3, TLSv1.2 if nginx >= 1.13.0
  ssl_prefer_server_ciphers on;
  ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
  # ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0, not compatible with import-videos script
  ssl_session_timeout  10m;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off; # Requires nginx >= 1.5.9
  ssl_stapling on; # Requires nginx >= 1.3.7
  ssl_stapling_verify on; # Requires nginx => 1.3.7

  # Configure with your resolvers
  # resolver $DNS-IP-1 $DNS-IP-2 valid=300s;
  # resolver_timeout 5s;

  # Enable compression for JS/CSS/HTML bundle, for improved client load times.
  # It might be nice to compress JSON, but leaving that out to protect against potential
  # compression+encryption information leak attacks like BREACH.
  # By default, we serve pre-compressed files first
  gzip on;
  gzip_types text/css text/html application/javascript;
  gzip_vary on;
  gzip_static on; # this will make nginx use the .gz files
  # Same goes for Brotli
  brotli on;
  brotli_comp_level 4;
  brotli_types text/plain text/css application/javascript application/json image/svg+xml application/xml+rss;
  brotli_static on; # this will make nginx use the .br files

  # Enable HSTS
  # Tells browsers to stick with HTTPS and never visit the insecure HTTP
  # version. Once a browser sees this header, it will only visit the site over
  # HTTPS for the next 2 years: (read more on hstspreload.org)
  #add_header Strict-Transport-Security "max-age=63072000; includeSubDomains";

  access_log  /var/log/nginx/olki.example.org.log;
  error_log   /var/log/nginx/olki.example.org.error.log debug;

  location ^~ '/.well-known/acme-challenge' {
    default_type "text/plain";
    root /var/www/certbot;
  }

  # Bypass OLKi to increase performance when serving the client. Could be removed
  location ~ ^/client/(.*\.(js|css|woff2|otf|ttf|woff|eot))$ {
    add_header Cache-Control "public, max-age=31536000, immutable";

    alias /var/www/peertube/peertube-latest/client/dist/$1;
  }

  # if the uri is not found, look for index.html, else pass everthing to gunicorn
  location / {
    index index.html;
    try_files $uri $uri/
    @uvicorn;
  }

  location @uvicorn {
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_redirect off;

    proxy_pass http://127.0.0.1:5000;
  }

  client_max_body_size 100M;
}

server {
  listen 80;
  listen [::]:80;
  server_name olki.example.org;

  access_log /var/log/nginx/olki.example.org.access.log;
  error_log /var/log/nginx/olki.example.org.error.log;

  location /.well-known/acme-challenge/ {
    default_type "text/plain";
    root /var/www/certbot;
  }
  location / { return 301 https://$host$request_uri; }
}
