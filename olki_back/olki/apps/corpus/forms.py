import json
import logging
from django import forms

from .models import Corpus as CorpusModel


logger = logging.getLogger(__name__)


class CorpusForm(forms.ModelForm):
    class Meta:
        model = CorpusModel
        fields = (
            'title',
            'description',
            'digital_object_identifier',
            'cloned_from',
            'license',
            'language',
            'draft',

            'related',

            'comments_allowed',
            'comments_only_local',
            'comments_only_visible_if_approved',

            'original_publication_at',
        )

    tags = forms.CharField(required=False)
    authors = forms.CharField(required=False)

    def clean_title(self):
        return ' '.join(self.cleaned_data.get('title').split()).replace('\n', ' ')

    def clean_tags(self):
        return json.loads(self.cleaned_data.get('tags') or '[]')

    def clean_authors(self):
        return json.loads(self.cleaned_data.get('authors') or '[]')

    def save(self, commit=True):
        corpus = super(CorpusForm, self).save()  # Save the child so we have an ID for the m2m

        authors = self.cleaned_data.get('authors')
        if authors:
            corpus.create_or_update_authors(authors)

        tags = self.cleaned_data.get('tags')
        if tags:
            corpus.create_or_update_tags(tags)

        return corpus
