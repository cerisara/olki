# Generated by Django 2.2.3 on 2019-08-30 11:54

import django.contrib.postgres.search
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('oaipmh', '0001_initial'),
        ('corpus', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='corpusfile',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='corpus',
            name='repository',
            field=models.ForeignKey(blank=True, help_text='The repository that contains the corpus.', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='corpora', to='oaipmh.Repository'),
        ),
        migrations.AlterField(
            model_name='corpus',
            name='actor',
            field=models.ForeignKey(blank=True, help_text='The actor that deposited the corpus.', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='corpora_owned', to='federation.Actor'),
        ),
        migrations.AlterField(
            model_name='corpus',
            name='license',
            field=models.TextField(blank=True, help_text='An [SDPX license](https://spdx.org/licenses/) identifier.', null=True),
        ),
        migrations.AlterField(
            model_name='corpus',
            name='search_vector',
            field=django.contrib.postgres.search.SearchVectorField(blank=True, null=True),
        ),
    ]
