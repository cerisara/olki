from rolepermissions.permissions import register_object_checker
from olki.apps.account.roles import Moderator, Administrator


@register_object_checker()
def delete_corpus(role, user, corpus):
    if role == Moderator or role == Administrator:
        return True

    if user.actor.corpora_owned.filter(id=corpus.id).exists():
        return True

    return False
