import graphene
from django import forms
from graphene_django.types import ErrorType
from graphql_jwt.decorators import login_required

from olki.apps.core.graphql.upload.django.mutation import DjangoUploadModelFormMutation
from olki.apps.core.graphql.upload.scalars import Upload
from .models import Profile as ProfileModel


class ProfileForm(forms.ModelForm):
    class Meta:
        model = ProfileModel
        fields = (
            'name',
            'bio',
            'indexed',
            'private',
        )
        exclude = ('id',)


class ProfileMutation(DjangoUploadModelFormMutation):
    class Input:
        image = Upload()

    class Meta:
        form_class = ProfileForm

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        input['id'] = info.context.user.profile.id

        form = cls.get_form(root, info, **input)
        files = info.context.FILES

        if form.is_valid():
            mutation = cls.perform_mutate(form, info)
            if files:
                print(files)
                print(files['1'])
                info.context.user.profile.image.save(
                    files['1'].name,
                    files['1']
                )
            return mutation
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class ProfileMutations(graphene.AbstractType):
    editProfile = ProfileMutation.Field()
