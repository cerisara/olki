"""
This is a module expected by olki.apps.core.middleware.HeaderMiddleware

It registers pattern matchings not served by Django but by the frontend
SPA. This allows modifying the response in case a headless client queries
a route normally served by the SPA with no processing by Django.

Note: instead of views, routes in urlpatterns register functions that
take a request and a response in argument, to modify the response.
"""

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import path

from .models import Note
from olki.apps.federation.utils import is_federation_accept


def none(request, response):
    pass


def note(request, response, note_id):
    # we have an activitypub object request
    if is_federation_accept(request.headers['Accept']):
        c = get_object_or_404(Note, pk=note_id)
        return JsonResponse(c.to_json(), content_type="application/activity+json; charset=utf-8")  # FIXME: return Note representation in JSON-LD

    # we have a normal web page
    if '.' not in note_id:
        response['Link'] = ','.join([
            '<{}/note/{}>; rel="alternate"; type="application/activity+json"'.format(settings.INSTANCE_URL, note_id)
        ])
    return response


urlpatterns = [
    path('note/<str:note_id>', note, name="note"),
]
