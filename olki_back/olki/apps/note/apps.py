from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'olki.apps.note'

    def ready(self):
        pass
