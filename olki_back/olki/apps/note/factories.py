import factory
from faker import Faker

from django.apps import apps
from olki.fixtures.factories import registry, registry as f, NoUpdateOnCreate

fake = Faker()
app_names = [app.name for app in apps.app_configs.values()]
f.autodiscover(app_names)


@registry.register
class NoteFactory(NoUpdateOnCreate, factory.django.DjangoModelFactory):
    class Meta:
        model = "note.Note"

    content = factory.Faker("text")

    @factory.post_generation
    def post_save(self, create, extracted, **kwargs):
        user = f['account.User']()
        self.actor = user.actor
