from __future__ import unicode_literals

import channels_graphql_ws
import graphene
from django.apps import apps
from django.db import models
from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from graphene import relay, ID, Field, Union, String, Int, Boolean
from graphene.types.generic import GenericScalar
from graphene_django.filter.fields import DjangoFilterConnectionField, DjangoConnectionField
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required

from olki.apps.core.graphql.types import CountableConnectionBase
from olki.apps.core.graphql.utils.mixins import PKMixin
from olki.apps.corpus.models import Corpus
from .models import Activity as ActivityModel, InboxItem as InboxItemModel, Actor as ActorModel, Follow as FollowModel
from .routes import outbox
from .serializers import ActorSerializer


class Activity(PKMixin, DjangoObjectType):
    class Meta:
        model = ActivityModel
        filter_fields = {
            'type': ['exact'],
        }
        exclude_fields = [
            'payload',
        ]
        description = "This is an Activity as defined in the ActivityStreams Vocabulary."
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

    class TargetUnion(Union):
        """
        A list of possible models that can be considered as targets.
        """

        class Meta:
            types = [Corpus]

    payload = GenericScalar()
    inReplyTo = GenericScalar()
    actor = Field('olki.apps.account.schema.Actor')
    objectDeleted = Boolean()

    def resolve_inReplyTo(self, info):
        inReplyTo = ActivityModel.objects.filter(payload__object__id=self.payload['object'].get('inReplyTo')).first()
        return inReplyTo and {
            'id': inReplyTo.id,
            'actor': ActorSerializer(inReplyTo.actor).data
        } or None

    def resolve_payload(self, info):
        return self.payload

    def resolve_objectDeleted(self, info):
        return self.object_id and not self.object

    @classmethod
    def get_queryset(cls, queryset, info):
        return queryset


class CountableInboxItem(CountableConnectionBase):
    """Connection subclass that supports unread/read counts in addition to total count."""

    class Meta:
        abstract = True

    total_read_count = graphene.Int()
    total_unread_count = graphene.Int()

    def resolve_total_read_count(self, info, **kwargs):
        if isinstance(self.iterable, models.query.QuerySet):
            return self.iterable.filter(is_read=True).count()
        return len(self.iterable)

    def resolve_total_unread_count(self, info, **kwargs):
        if isinstance(self.iterable, models.query.QuerySet):
            return self.iterable.filter(is_read=False).count()
        return len(self.iterable)


class InboxItem(PKMixin, DjangoObjectType):
    class Meta:
        model = InboxItemModel
        filter_fields = {
            'is_read': ['exact'],
        }
        description = "This is an InboxItem, a store for activities binding to local actors, with read/unread status."
        interfaces = (relay.Node,)
        connection_class = CountableInboxItem

    url = String()
    actor = Field('olki.apps.account.schema.Actor')

    def resolve_url(self, info):
        return self.activity.object.fid if self.activity.object else None


class ActivityQuery(object):
    # Activities related to a corpus
    activities_corpus = DjangoFilterConnectionField(
        Activity,
        pk=ID(required=True, description=_('A Flax ID that is used as a primary key for the corpus.')),
        description=_('List the Activities related to a given Corpus.')
    )

    def resolve_activities_corpus(self, info, pk, **kwargs):
        from django.contrib.contenttypes.models import ContentType
        return (
            ActivityModel.objects.filter(target_id=pk).order_by('-creation_date')
            |
            ContentType.objects.get(model='corpus').objecting_activities.filter(object_id=pk).order_by('-creation_date')
        )

    # Activities related to an actor
    activities_actor = DjangoFilterConnectionField(
        Activity,
        fid=ID(required=True, description=_('Federation ID (a canonical URL) of the actor.')),
        description=_('List the Activities related to a given Actor.')
    )

    def resolve_activities_actor(self, info, fid, **kwargs):
        return ActivityModel.objects.filter(actor__fid=fid).order_by('-creation_date')

    # Activities related to a user
    # activities_user = DjangoFilterConnectionField(
    #     Activity,
    #     username=String(required=True),
    #     verbs=Argument(List(String), required=False,
    #                    description=_('A list of public Activity verbs you wish to limit your query to.')),
    #     description=_('List the Activities related to a given User.')
    # )
    notifications_for_user = DjangoFilterConnectionField(
        InboxItem,
        description=_('List the notification Activities addressed to the logged-in user.')
    )

    @login_required
    def resolve_notifications_for_user(self, info, **kwargs):
        return InboxItemModel.objects.filter(actor=info.context.user.actor).order_by('is_read', '-id')


class OnNewNotificationForUser(channels_graphql_ws.Subscription):
    """Subscription triggers on a new inbox message, for a logged-in user."""

    notification = Field(InboxItem)

    def subscribe(self, info):
        """Client subscription handler."""
        # Specify the subscription group client subscribes to.
        return ["USER_{}".format(info.context.user.pk)]

    def publish(self, info):
        """Called to prepare the subscription notification message."""

        # The `self` contains payload delivered from the `broadcast()`.
        new_notification = self["notification"]

        return OnNewNotificationForUser(
            notification=new_notification
        )

    @classmethod
    def new_notification(cls, notification, group=None):
        """Auxiliary function to send subscription notifications.

        No group (=None) means the notification will reach every user.
        """
        cls.broadcast(
            group=group,
            payload={"notification": notification},
        )

    @staticmethod
    @transaction.atomic
    def notify_username(username, content, summary=None):
        OnNewNotificationForUser.notify_user(ActorModel.objects.get(preferred_username=username).user, content, summary)

    @staticmethod
    @transaction.atomic
    def notify_user(user, content, summary=None, url=None):
        actor = user.actor
        domain_actor = ActorModel.objects.get(fid=ActorModel.get_actor_data('olki').get('fid'))
        # create Note
        obj = apps.get_model('note.Note')(
            content=content,
            summary=summary,
            actor=domain_actor,
            fid=url  # where to redirect on click to the notification
        )
        obj.save()
        # create the Announce (https://www.w3.org/TR/activitystreams-vocabulary/#dfn-announce) as Notification
        outbox.dispatch({"type": "Announce", "object": {"type": "Note"}}, context={"note": obj, "to_actor": actor})


@receiver(post_save, sender=InboxItemModel, dispatch_uid="create_notification_for_inbox_item")
def create_notification_for_inbox_item(sender, instance, created, **kwargs):
    # the instance actor doesn't have a user, so skip it
    if created and instance.actor.user:
        OnNewNotificationForUser.new_notification(
            instance,
            group="USER_{}".format(instance.actor.user.pk)
        )


class InboxSubscriptions(object):
    on_new_notification_for_user = OnNewNotificationForUser.Field()


class Follow(DjangoObjectType):
    class Meta:
        model = FollowModel
        exclude_fields = ('id',)
        only_fields = [
            'target',
            'modification_date',
        ]
        description = _('List the instances followed by that instance.')
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

    pk = Int()
    target = Field('olki.apps.account.schema.Actor')
    count = Int()

    def resolve_pk(self, info):
        return self.id

    def resolve_count(self, info):
        return Corpus.remote.filter(actor__domain=self.target.domain).count()

    @classmethod
    def get_queryset(cls, queryset, info):
        return queryset\
            .filter(approved=True)\
            .filter(actor=ActorModel.objects.local().get(preferred_username='olki'))


class InstanceFollowsQuery(object):
    follows = DjangoConnectionField(
        Follow,
        description=_('List of instances followed by the current instance.')
    )
