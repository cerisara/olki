from datetime import timezone

import factory
from faker import Faker
from olki.fixtures.factories import registry, NoUpdateOnCreate

fake = Faker()


@registry.register
class DomainFactory(NoUpdateOnCreate, factory.django.DjangoModelFactory):
    class Meta:
        model = "federation.Domain"
        django_get_or_create = ('name',)

    name = "test.federation"


@registry.register
class ActorFactory(NoUpdateOnCreate, factory.django.DjangoModelFactory):
    class Meta:
        model = "federation.Actor"

    preferred_username = factory.Faker('user_name')
    name = factory.Faker('sentence', locale='la')
    summary = factory.Sequence(lambda n: "\n".join(fake.paragraphs(nb=1)))
    creation_date = factory.Faker("past_datetime", start_date="-10y", tzinfo=timezone.utc)
    domain = factory.SubFactory(DomainFactory)

    @factory.post_generation
    def post_save(self, create, extracted, **kwargs):
        #args = apps.get_model('federation', 'Actor').get_actor_data(self.preferred_username)
        pass

