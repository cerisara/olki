from typing import Iterator, List

import unicodedata
import re
from django.conf import settings

from olki.apps.core import session

from . import signing


def full_url(path):
    """
    Given a relative path, return a full url usable for federation purposes
    """
    if path.startswith("http://") or path.startswith("https://"):
        return path
    root = settings.INSTANCE_URL

    if path.startswith("/") and root.endswith("/"):
        return root + path[1:]
    if not path.startswith("/") and not root.endswith("/"):
        return root + "/" + path

    return root + path


def clean_wsgi_headers(raw_headers):
    """
    Convert WSGI headers from CONTENT_TYPE to Content-Type notation
    """
    cleaned = {}
    non_prefixed = ["content_type", "content_length"]
    for raw_header, value in raw_headers.items():
        h = raw_header.lower()
        if not h.startswith("http_") and h not in non_prefixed:
            continue

        words = h.replace("http_", "", 1).split("_")
        cleaned_header = "-".join([w.capitalize() for w in words])
        cleaned[cleaned_header] = value

    return cleaned


def slugify_username(username):
    """
    Given a username such as "hello M. world", returns a username
    suitable for federation purpose (hello_M_world).

    Preserves the original case.

    Code is borrowed from django's slugify function.
    """

    value = str(username)
    value = (
        unicodedata.normalize("NFKD", value).encode("ascii", "ignore").decode("ascii")
    )
    value = re.sub(r"[^\w\s-]", "", value).strip()
    return re.sub(r"[-\s]+", "_", value)


def is_local(url, local_hostname=settings.INSTANCE_HOSTNAME):
    if not url:
        return True

    return url.startswith("http://{}/".format(local_hostname)) or url.startswith(
        "https://{}/".format(local_hostname)
    )


def is_federation_accept(header):
    if (
            'application/ld+json' in header
            or
            'application/activity+json' in header
    ):
        return True
    return False


def retrieve_activities_in_collection(
        collection_url,
        filter_activity_type_in=[],
        filter_object_type_in=[],
        last_seen_id=None
) -> List[dict]:
    """
    Given a paginated collection url, returns a list of activities

    :param collection_url: a paginated collection url (i.e. inbox, outbox)
    :param filter_activity_type_in: a list of activity types the results have to match one of
    :param filter_object_type_in: a list of object types the results have to match one of
    :param last_seen_id: fid of the latest Activity seen from that Actor, allowing to stop early
    :return: List[dict]
    """
    from urllib.parse import urlparse, parse_qs

    def filter_activities(activities: List[dict]) -> List[dict]:
        _activities = activities
        if filter_activity_type_in:
            _activities = [
                a
                for a in _activities
                if a['type'] in filter_activity_type_in
            ]
        if hasattr(_activities, 'object') and filter_object_type_in:
            _activities = [
                a
                for a in _activities
                if a['object']['type'] in filter_object_type_in
            ]
        return _activities

    first_page = session.get_session().get(collection_url + '?page=1').json()
    stopped_iteration = False

    def check_if_should_stop_iteration(_activities):
        if last_seen_id:
            last_seen_activity = next(obj for obj in _activities if obj['id'] == last_seen_id)
            if last_seen_activity:
                return _activities[:_activities.index(last_seen_activity)], True
        return _activities, False

    activities, stopped_iteration = check_if_should_stop_iteration(first_page['orderedItems'])
    activities = filter_activities(activities)
    activities = [activities]

    last = first_page['last']
    qs = parse_qs(urlparse(last).query)
    num_pages = int(qs['page'][0])

    if not stopped_iteration and num_pages > 1:
        for page in range(2, num_pages):
            next_page = session.get_session().get(collection_url, params={'page': page}).json()
            _activities, stopped_iteration = check_if_should_stop_iteration(next_page['orderedItems'])
            activities.append(filter_activities(_activities))
            if stopped_iteration:
                break

    import operator
    from functools import reduce
    activities = reduce(operator.add, activities)  # from [[...], [...], etc.] to [...]
    activities.reverse()  # keep activities chronological
    return activities


def retrieve_ap_object(
    fid, actor, serializer_class=None, queryset=None, apply_instance_policies=True
):
    # we have a duplicate check here because it's less expensive to do those checks
    # twice than to trigger a HTTP request
    # payload, updated = mrf.inbox.apply({"id": fid})
    # if not payload:
    #     raise exceptions.BlockedActorOrDomain()
    if queryset:
        try:
            # queryset can also be a Model class
            existing = queryset.filter(fid=fid).first()
        except AttributeError:
            existing = queryset.objects.filter(fid=fid).first()
        if existing:
            return existing

    auth = (
        None if not actor else signing.get_auth(actor.private_key, actor.private_key_id)
    )
    response = session.get_session().get(
        fid,
        auth=auth,
        timeout=5,
        verify=settings.EXTERNAL_REQUESTS_VERIFY_SSL,
        headers={
            "Accept": "application/activity+json",
            "Content-Type": "application/activity+json",
        },
    )
    response.raise_for_status()
    data = response.json()

    # we match against mrf here again, because new data may yield different
    # results
    # data, updated = mrf.inbox.apply(data)
    # if not data:
    #     raise exceptions.BlockedActorOrDomain()

    if not serializer_class:
        return data
    serializer = serializer_class(data=data, context={"fetch_actor": actor})
    serializer.is_valid(raise_exception=True)
    return serializer.save()
