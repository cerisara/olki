# pylint: skip-file

"""py
    Original author: Eliot Berriot
    Project of origin: Funkwhale
    Commit of origin: 49769819265170a771e747b8696de6a27a100379
    Project licence: AGPLv3.0 or later

The module registers "routes" in the form of object matchings
that are triggered when a dispatched message matches the type
and potential 'object.type'. See the Route.register function
in `olki.apps.federation.activity`.

Don't forget a route is usually can be registered twice: once
for the inbox router, and once for the outbox (corresponding
to deserializing/serializing messages, resp.).

--- How/what to serialize?
You need to yield a dict that the dispatch can later transform
into an Activity. It will hold the links to actors, objects,
targets, etc. in the database, but that's not what is sent on
the wire! Essentially what is sent is the `payload` property of
that dict. You need to serialize the payload, but what is the
payload? It contains:
- a serialized verb (Follow, Create, etc.) with a context:
    - a serialized object, for instance (Note, Corpus, etc.)

--- What gets sent on the network?
In the end, just the payload.
"""
import logging

from typing import Generator
from django.urls import reverse
from django.apps import apps
from olki.apps.federation.utils import full_url
from . import activity
from . import models
from . import serializers

logger = logging.getLogger(__name__)
inbox = activity.InboxRouter()
outbox = activity.OutboxRouter()


def with_recipients(payload, to=[], cc=[]):
    if to:
        payload["to"] = to
    if cc:
        payload["cc"] = cc
    return payload


@inbox.register({"type": "Follow"})
def inbox_follow(payload, context):
    logger.debug(context)
    context["recipient"] = [
        ii.actor for ii in context["inbox_items"] if ii.type == "to"
    ][0]
    serializer = serializers.FollowSerializer(data=payload, context=context)
    if not serializer.is_valid(raise_exception=context.get("raise_exception", False)):
        logger.debug(
            "Discarding invalid follow from {}: %s",
            context["actor"].fid,
            serializer.errors,
        )
        return

    autoapprove = serializer.validated_data["object"].should_autoapprove_follow(
        context["actor"]
    )
    logger.debug("Autoapprove: %s", autoapprove)
    follow = serializer.save(approved=True if autoapprove else None)
    if follow.approved:
        outbox.dispatch({"type": "Accept"}, context={"follow": follow})
    return {"object": follow.target, "related_object": follow}


@inbox.register({"type": "Accept"})
def inbox_accept(payload, context):
    logger.debug(context)
    context["recipient"] = [
        ii.actor for ii in context["inbox_items"] if ii.type == "to"
    ][0]
    serializer = serializers.AcceptFollowSerializer(data=payload, context=context)
    if not serializer.is_valid(raise_exception=context.get("raise_exception", False)):
        logger.debug("Discarding invalid accept from {actor_fid}: {errors}".format(
            actor_fid=context["actor"].fid,
            errors=serializer.errors
        ))
        return

    serializer.save()
    follow = serializer.validated_data["follow"]
    return {"object": follow, "related_object": follow.target}


@outbox.register({"type": "Accept"})
def outbox_accept(context):
    follow = context["follow"]
    if not isinstance(follow, models.Follow):
        raise TypeError("outbox dispatch follow should have an instance of model Follow in its context")

    # the input follow is the follow we answer to, not the one we generate. As a reply, we invert actors and targets
    actor = follow.target
    target = follow.actor

    payload = serializers.AcceptFollowSerializer(follow, context={"actor": actor}).data
    yield {
        "actor": actor,
        "type": "Accept",
        "payload": with_recipients(payload, to=[target]),
        "object": target
    }


@inbox.register({"type": "Undo", "object.type": "Follow"})
def inbox_undo_follow(payload, context):
    serializer = serializers.UndoFollowSerializer(data=payload, context=context)
    if not serializer.is_valid(raise_exception=context.get("raise_exception", False)):
        logger.debug(
            "Discarding invalid follow undo from %s: %s",
            context["actor"].fid,
            serializer.errors,
        )
        return

    serializer.save()


@outbox.register({"type": "Undo", "object.type": "Follow"})
def outbox_undo_follow(context):
    follow = context["follow"]
    if not isinstance(follow, models.Follow):
        raise TypeError("outbox dispatch follow should have an instance of model Follow in its context")

    actor = follow.actor
    recipient = follow.target

    payload = serializers.UndoFollowSerializer(follow, context={"actor": actor}).data
    yield {
        "actor": actor,
        "type": "Undo",
        "payload": with_recipients(payload, to=[recipient]),
        "object": recipient
    }


from django.db.models.signals import pre_delete
from django.dispatch import receiver


@receiver(pre_delete)
def delete_repo(sender, instance, **kwargs):
    if sender == models.Follow:
        outbox.dispatch({"type": "Undo", "object.type": "Follow"}, context={"follow": instance})


@outbox.register({"type": "Follow"})
def outbox_follow(context):
    follow = context["follow"]
    if not isinstance(follow, models.Follow):
        raise TypeError("outbox dispatch follow should have an instance of model Follow in its context")


    payload = serializers.FollowSerializer(follow, context={"actor": follow.actor}).data
    yield {
        "type": "Follow",
        "actor": follow.actor,
        "payload": with_recipients(payload, to=[follow.target]),
        "object": follow.target,
        "related_object": follow,
    }


##
# `Create` types below
##


# works with i.e. `{"type": "Create", "object": {"type": "Corpus"}}, context={"corpus": corpus}`
# in for example activity.OutboxRouter.dispatch
# i.e. `outbox.dispatch({"type": "Create", "object": {"type": "Corpus"}}, context={"corpus": corpus})`
@outbox.register({"type": "Create", "object.type": "Corpus"})
def outbox_create_corpus(context):
    return outbox_create_or_update_corpus("Create", context)


@outbox.register({"type": "Update", "object.type": "Corpus"})
def outbox_update_corpus(context):
    return outbox_create_or_update_corpus("Update", context)


def outbox_create_or_update_corpus(atype: str, context) -> Generator:
    corpus = context["corpus"]
    corpus_data = serializers.CorpusSerializer(corpus).data
    a = serializers.ActivitySerializer(
        {"type": atype, "object": corpus_data, "actor": corpus.actor}
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": atype,
        "actor": corpus.actor,
        "payload": with_recipients(
            a.data,
            cc=[
                {
                    "type": "followers",
                    "target": models.Actor.objects.local().get(preferred_username="olki")
                },
                {
                    "type": "followers",
                    "target": corpus.actor
                }
            ],
            to=[activity.PUBLIC_ADDRESS]
        ),
        "object": corpus
    }


@inbox.register({"type": "Create", "object.type": "Corpus"})
@inbox.register({"type": "Update", "object.type": "Corpus"})
def inbox_create_or_update_corpus(payload, context):
    CorpusModel = apps.get_model("corpus.Corpus")
    serializer = None
    a = context.get("activity")

    if payload["type"] == "Create":
        serializer = serializers.CorpusSerializer(
            data=payload["object"],
            context={"activity": a, "actor": context["actor"]},
        )
    else:
        try:
            corpus = CorpusModel.remote.get(fid=payload["object"]["id"])
            serializer = serializers.CorpusSerializer(
                corpus,
                data=payload["object"],
                context={"activity": a, "actor": context["actor"]},
            )
        except CorpusModel.DoesNotExist as e:
            logger.exception(e)
        except Exception as e:
            raise e


    if not serializer or not serializer.is_valid(raise_exception=context.get("raise_exception", True)):
        logger.warning("Discarding invalid Corpus {}".format(payload["type"]))
        return

    corpus = serializer.save()
    return {"object": corpus}


# works with i.e. `{"type": "Create", "object": {"type": "Note"}}, context={"note": note}`
# in for example activity.OutboxRouter.dispatch
# i.e. `outbox.dispatch({"type": "Create", "object": {"type": "Note"}}, context={"note": note})`
@outbox.register({"type": "Create", "object.type": "Note"})
def outbox_create_note(context):
    note = context["note"]
    note_data = serializers.NoteSerializer(note).data
    a = serializers.ActivitySerializer(
        {"type": "Create", "object": note_data, "actor": note.actor},
        # fid="{}/note/{}/activity".format(settings.INSTANCE_URL, note.id)
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": "Create",
        "actor": note.actor,
        "payload": with_recipients(
            a.data,
            # TODO: notify more than just the corpus author, but also the mentioned and replied-to actors.
            to=[note.corpus.actor]
        ),
        "object": note,
        "target": note.corpus
    }


@outbox.register({"type": "Announce", "object.type": "Note"})
def outbox_announce_note(context):
    """
    Announces are notifications within OLKi
    """
    note = context["note"]
    note_data = serializers.NoteSerializer(note).data
    to_actor = context["to_actor"]
    a = serializers.ActivitySerializer(
        {"type": "Announce", "object": note_data, "actor": note.actor},
        # fid="{}/note/{}/activity".format(settings.INSTANCE_URL, note.id)
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": "Announce",
        "actor": note.actor,
        "payload": with_recipients(
            a.data,
            to=[to_actor]
        ),
        "object": note,
        "target": to_actor
    }


@inbox.register({"type": "Create", "object.type": "Note"})
def inbox_create_note(payload, context):
    serializer = serializers.NoteSerializer(
        data=payload["object"],
        context={"activity": context.get("activity"), "actor": context["actor"]},
    )

    if not serializer.is_valid(raise_exception=context.get("raise_exception", True)):
        logger.warning("Discarding invalid Note create")
        return

    note = serializer.save()

    return {"object": note, "target": note.corpus}


@outbox.register({"type": "Update", "object.type": "Actor"})
def outbox_update_actor(context):
    actor = context["actor"]
    actor_data = serializers.ActorSerializer(actor).data
    a = serializers.ActivitySerializer(
        {"type": "Update", "object": actor_data, "actor": actor}
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": "Update",
        "actor": actor,
        "payload": with_recipients(
            a.data,
            # TODO: notify followers, and people who interacted with the actor's corpora
            cc=[
                full_url(
                    reverse(
                        "federation:actor-followers",
                        kwargs={"preferred_username": actor.preferred_username},
                    )
                )
            ]
        ),
        "object": actor,
        "target": actor
    }

##
# `Delete` types below
##


@outbox.register(
    {
        "type": "Delete",
        "object.type": [
            "Tombstone",
            "Actor",
            "Person",
            "Application",
            "Organization",
            "Service",
            "Group",
        ],
    }
)
def outbox_delete_actor(context):
    actor = context["actor"]
    serializer = serializers.ActivitySerializer(
        {"type": "Delete", "object": {"type": actor.type, "id": actor.fid}}
    )
    yield {
        "type": "Delete",
        "actor": actor,
        "payload": with_recipients(
            serializer.data,
            to=[activity.PUBLIC_ADDRESS, {"type": "instances_with_followers"}],
        ),
    }


@inbox.register(
    {
        "type": "Delete",
        "object.type": [
            "Tombstone",
            "Actor",
            "Person",
            "Application",
            "Organization",
            "Service",
            "Group",
        ],
    }
)
def inbox_delete_actor(payload, context):
    actor = context["actor"]
    serializer = serializers.DeleteSerializer(data=payload)
    if not serializer.is_valid():
        logger.info("Skipped actor %s deletion, invalid payload", actor.fid)
        return

    deleted_fid = serializer.validated_data["fid"]
    try:
        # ensure the actor only can delete itself, and is a remote one
        actor = models.Actor.objects.local(False).get(fid=deleted_fid, pk=actor.pk)
    except models.Actor.DoesNotExist:
        logger.warning("Cannot delete actor %s, no matching object found", actor.fid)
        return


@outbox.register({"type": "Delete", "object.type": "Corpus"})
def outbox_delete_corpus(context):
    corpus = context["corpus"]
    serializer = serializers.ActivitySerializer(
        {"type": "Delete", "object": {"type": "Corpus", "id": corpus.fid}, "actor": corpus.actor}
    )
    yield {
        "type": "Delete",
        "actor": corpus.actor,
        "payload": with_recipients(
            serializer.data,
            cc=[
                {
                    "type": "followers",
                    "target": models.Actor.objects.local().get(preferred_username="olki")
                },
                {
                    "type": "followers",
                    "target": corpus.actor
                }
            ],
            to=[activity.PUBLIC_ADDRESS, {"type": "instances_with_followers"}],
        ),
    }


@inbox.register({"type": "Delete", "object.type": "Corpus"})
def inbox_delete_corpus(payload, context):
    CorpusModel = apps.get_model("corpus.Corpus")

    corpus = context.get("activity").payload["object"]
    actor = context.get("actor")
    serializer = serializers.DeleteSerializer(data=payload)
    if not serializer.is_valid():
        logger.info("Skipped Corpus %s deletion, invalid payload", corpus.id)
        return

    deleted_fid = serializer.validated_data["fid"]
    try:
        # ensure the actor only can delete itself, and is a remote one
        corpus = CorpusModel.remote.get(fid=deleted_fid, actor=actor)
        corpus.delete()
    except CorpusModel.DoesNotExist:
        logger.warning("Cannot delete Corpus %s, no matching object found", deleted_fid)
        return


@outbox.register({"type": "Delete", "object.type": "Note"})
def outbox_delete_note(context):
    note = context["note"]
    serializer = serializers.ActivitySerializer(
        {"type": "Delete", "object": {"type": "Note", "id": note.fid}, "actor": note.actor}
    )
    yield {
        "type": "Delete",
        "actor": note.actor,
        "payload": with_recipients(
            serializer.data,
            to=[activity.PUBLIC_ADDRESS, {"type": "instances_with_followers"}],
        ),
    }
    # TODO: test delete outgoing/outbox for Note objects


@inbox.register({"type": "Delete", "object.type": "Note"})
def inbox_delete_note(payload, context):
    NoteModel = apps.get_model("note.Note")

    note = context.get("activity").payload["object"]
    actor = context.get("actor")
    serializer = serializers.DeleteSerializer(data=payload)
    if not serializer.is_valid():
        logger.info("Skipped Note %s deletion, invalid payload", note.id)
        return

    deleted_fid = serializer.validated_data["fid"]
    try:
        # ensure the actor only can delete itself, and is a remote one
        note = NoteModel.remote.get(fid=deleted_fid, actor=actor)
        note.delete()
    except NoteModel.DoesNotExist:
        logger.warning("Cannot delete Note %s, no matching object found", deleted_fid)
        return
