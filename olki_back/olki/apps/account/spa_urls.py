from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import reverse, get_object_or_404
from django.urls import path

from .models import User
from olki.apps.federation.utils import is_federation_accept


def profile(request, response, user_id):
    # we have an activitypub object request
    if is_federation_accept(request.headers['Accept']):
        u = get_object_or_404(User, username=user_id)
        url = reverse('federation:actor-detail', kwargs={'preferred_username': u.actor.preferred_username})
        return HttpResponseRedirect(url)

    # we have a normal web page
    if '.' not in user_id:
        response['Link'] = ','.join([
            '<{}/@{}.rss>; rel="alternate"; type="application/rss+xml"'.format(settings.INSTANCE_URL, user_id),
            '<{}/@{}.atom>; rel="alternate"; type="application/atom+xml"'.format(settings.INSTANCE_URL, user_id),
            '<{}/federation/actor/{}>; rel="alternate"; type="application/activity+json"'.format(settings.INSTANCE_URL, user_id),
            '<{}/.well-known/webfinger?resource=acct%3A{}%40{}>; rel="lrdd"'.format(settings.INSTANCE_URL, user_id, settings.INSTANCE_HOSTNAME)
        ])
    return response


urlpatterns = [
    path('@<str:user_id>', profile, name="profile"),
]
