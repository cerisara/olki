from django.urls import path

from . import views


urlpatterns = [
    # syndication feeds
    path('@<str:user_id>.rss', views.rss(), name='rss'),
    path('@<str:user_id>.atom', views.atom(), name='atom'),
]