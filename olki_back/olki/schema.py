import channels_graphql_ws
import django
import graphene
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required
from graphql_jwt.shortcuts import get_user_by_token

from olki.apps.corpus.schema import CorpusModel
from olki.apps.note.schema import NoteModel

# keeping imports of the corpus and note schemas
MODELS = [
    CorpusModel,
    NoteModel
]

from olki.apps.account.mutations import UserMutations
from olki.apps.account.schema import Viewer, UserQuery, ActorQuery
from olki.apps.core.mutations import CoreMutations
from olki.apps.corpus.mutations import CorpusMutations
from olki.apps.corpus.schema import CorpusQuery
from olki.apps.federation.schema import ActivityQuery, InboxSubscriptions, InstanceFollowsQuery
from olki.apps.federation.mutations import FederationMutations
from olki.apps.note.mutations import NoteMutations
from olki.apps.oaipmh.mutations import OaipmhMutations
from olki.apps.profile.mutations import ProfileMutations


class Queries(
    UserQuery,
    ActorQuery,
    CorpusQuery,
    ActivityQuery,
    InstanceFollowsQuery,
    graphene.ObjectType
):
    """Queries wrapper
    This class inherits from multiple Queries
    as we add more apps queries to our project

    Note it is an instance (graphene.ObjectType)
    inheriting from interfaces (graphene.AbstractType)
    """
    if settings.DEBUG:
        from graphene_django.debug import DjangoDebug
        debug = graphene.Field(DjangoDebug, name='debug')

    viewer = graphene.Field(Viewer, description=_(
        'The viewer field represents the currently logged-in user.'
        'Its subfields expose data that are contextual to the user.'
    ))

    @login_required
    def resolve_viewer(self, info, **kwargs):
        return info.context.user


class Mutations(
    CoreMutations,
    UserMutations,
    ProfileMutations,
    CorpusMutations,
    NoteMutations,
    OaipmhMutations,
    FederationMutations,
    graphene.ObjectType
):
    """Mutations wrapper
    This class inherits from multiple Mutations
    as we add more apps mutations to our project

    Note it is an instance (graphene.ObjectType)
    inheriting from interfaces (graphene.AbstractType)
    """
    pass


class Subscriptions(
    InboxSubscriptions,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(
    query=Queries,
    mutation=Mutations,
    subscription=Subscriptions
)


class OLKiGraphqlWsConsumer(channels_graphql_ws.GraphqlWsConsumer):
    """Channels WebSocket consumer which provides GraphQL API."""
    schema = schema

    async def on_connect(self, payload):
        """New client connection handler."""

        # Since Firefox is not able to send proper HTTP/Websocket Authorization
        # headers, we have to rely on payload token to authentify the request.
        if payload:
            self.scope["user"] = get_user_by_token(payload.get("authorization").split(" ")[1])

        # We should `raise` from here to reject the connection.


# ------------------------------------------------------------------------- ASGI ROUTING

application = ProtocolTypeRouter(
    {
        # (olki.urls is added by default)
        "websocket": AuthMiddlewareStack(
            URLRouter([
                django.urls.path("graphql", OLKiGraphqlWsConsumer),
            ])
        ),
    }
)
