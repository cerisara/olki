"""
ASGI config for the olki project.

It exposes the ASGI callable as a module-level variable named ``application``.
"""

import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'olki.settings')
os.environ.setdefault("ASGI_THREADS", "5")

import django  # noqa

django.setup()

from .schema import application  # noqa
