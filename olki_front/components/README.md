# COMPONENTS

The components directory contains Vue.js Components.

The `sublayouts` directory contains components used in practice as layouts for their slots. They are often used as dynamic components within the `default` layout. It allows for the page to need no full refresh upon change outside of nested routes (i.e.: index, social, upload).

The `styled` directory contains components styled via JS (vue-styled-components).

_Nuxt.js doesn't supercharge these components._
