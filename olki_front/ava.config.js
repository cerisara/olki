export default {
  require: ['./test/helpers/setup.js'],
  helpers: ['**/helpers/**/*'],
  sources: ['**/*.{js,vue}'],
  snapshotDir: './test/snapshot'
}
