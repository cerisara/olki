# OLKi FE

The frontend is meant to run on its own lightweight server but points to the API.

## Browser support

OLKi FE supports the latest versions of the following browsers:

- Chrome
- Edge
- Firefox
- Safari

Compatible versions of each (Opera, Brave, Samsung, etc.) should be fine.

## Goals and non-goals

### Goals

- Support the most common use cases
- Small page weight
- Fast even on low-end devices
- Accessibility
- Support latest versions of Chrome, Edge, Firefox, and Safari
- Admin/moderation panel

### Secondary / possible future goals

- Support for non-English languages (i18n)
- Offline search
- Keyboard shortcuts

### Non-goals

- Supporting old browsers, proxy browsers, or text-based browsers
- React Native / NativeScript / hybrid-native version
- Android/iOS apps (using Cordova or similar)
- Full functionality with JavaScript disabled
- Multi-instance support
- Offline support

## Building

OLKI FE requires [Node.js](https://nodejs.org/en/) v8+ and `npm`.

To build it for production (from OLKi's root):

```bash
make deps-prod-fe build-fe
```

### Updating

To keep your version of OLKI FE up to date, you can use `make` and `git` to check out the latest release (from OLKi's root):

```bash
make update-release
```

### Adding dependencies

Since the application is not using SSR and is just an SPA, dependencies are all
development dependencies (there is no runtime but the browser). Please place all
dependencies in devDependencies.

Please use `npx fixpack` to reorder the `package.json` after all changes to the
dependencies.

### Components used

We use the lightweight [Spectre.css](https://picturepan2.github.io/spectre/index.html) framework
and its beautiful components. [Zutre](https://maclisowski.github.io/zutre/#/) for the Vue components using Spectre.css. Thanks!

