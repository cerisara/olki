/**
 * Corpora is a module to cache information regarding the currently open corpora,
 * and the currently pending creation corpus.
 */

import createCorpusGql from '~/apollo/mutations/createCorpus'
import editCorpusGql from '~/apollo/mutations/editCorpus'
import deleteCorpusGql from '~/apollo/mutations/deleteCorpus'

export const state = () => ({
  data: {},
  openCorpora: [],
  current: {
    files: []
  }
})

export const mutations = {
  addOpenCorpora(state, data) {
    if (!state.openCorpora.find(corpus => corpus.pk === data.pk))
      state.openCorpora.push(data)
  },
  removeOpenCorpora(state, data) {
    state.openCorpora = state.openCorpora.filter(
      corpus => corpus.pk !== data.pk
    )
  },
  addCurrentFile(state, data) {
    state.current.files.push(data)
  },
  removeCurrentFile(state, data) {
    state.current.files = state.current.files.filter(
      file => file.id !== data.id
    )
  },
  removeCurrentFileID(state, data) {
    state.current.files = state.current.files.filter(file => file.id !== data)
  },
  resetCurrent(state) {
    state.current = {
      files: []
    }
  }
}

export const getters = {
  openCorpora(state) {
    return state.openCorpora
  },
  current(state) {
    return state.current
  }
}

export const actions = {
  INDEX_REFRESH({ commit, state }, input) {},
  async createCorpus({ commit, state }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: createCorpusGql,
          variables: input
        })
        .then(({ data }) => data.createCorpus)
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async editCorpus({ commit, state }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: editCorpusGql,
          variables: input
        })
        .then(({ data }) => data.editCorpus)
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async deleteCorpus({ commit }, pk) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: deleteCorpusGql,
          variables: { pk }
        })
        .then(({ data }) => data.deleteCorpus)
      return res
    } catch (e) {
      console.log(e)
    }
  }
}
