import common from './commonToAllClients'

export default function(context) {
  // prettier-ignore
  return {
    ...common(context),
    wsEndpoint: (
        context.env.baseURL.replace(/^https?/i, 'ws') ||
        `${window.location.protocol === 'https:' ? 'wss' : 'ws'}://${window.location.host}`
      ) + '/graphql',
    httpLinkOptions: {
      credentials: 'include'
    }
  }
}
