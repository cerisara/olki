import common from './commonToAllClients'

export default function(context) {
  return {
    ...common(context),
    wsEndpoint: null,
    getAuth: () => undefined
  }
}
