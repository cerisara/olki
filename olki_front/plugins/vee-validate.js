/* eslint-disable */
import Vue from 'vue'
import {
  Validator,
  install as VeeValidate
} from 'vee-validate/dist/vee-validate.minimal.esm.js'
import {
  required,
  min,
  max,
  dimensions,
  alpha,
  alpha_spaces,
  image,
  ext,
  is,
  date_format,
  email,
  date_between,
  url
} from 'vee-validate/dist/rules.esm.js'
import veeEn from 'vee-validate/dist/locale/en'

// Add the rules you need.
Validator.extend('required', required)
Validator.extend('alpha', alpha)
Validator.extend('alpha_spaces', alpha_spaces)
Validator.extend('dimensions', dimensions)
Validator.extend('image', image)
Validator.extend('email', email)
Validator.extend('ext', ext)
Validator.extend('min', min)
Validator.extend('max', max)
Validator.extend('is', is)
Validator.extend('date_format', date_format)
Validator.extend('date_between', date_between)
Validator.extend('url', url)

// Customize date_between
const dictionary = {
  en: {
    custom: {
      corpusOriginalPublicationDate: {
         'date_between': (field, [min, max]) => `The ${field} must be between ${min} (<em class="tooltip tooltip-bottom" data-tooltip="Often regarded as the first journal publication,\nthe Journal des sçavans inspired modern\nscientific publications and peer review">why?</em>) and ${max}.`
      }
    }
  }
}

// Merge the messages.
Validator.localize('en', veeEn)

Vue.use(VeeValidate, {
  inject: false, // augments performance by not injecting unless explicitly told to as follows
  dictionary
})

/* Injection snippet to add to components:
  $_veeValidate: {
    validator: 'new'
  },
*/
